import os
import re
import random
from shutil import copyfile

class RandomPhotosPicker():
	def __init__(self):
		self.listFilenames = []
		self.filenamesPicked = []
		self.numberOfFiles = None
		self.currentDirectory = None

	def main(self):

		self.currentDirectory = self.getCurrentDirectory()
		self.listFilenames = self.getFilenamesWithPath(self.currentDirectory)

		self.numberOfFiles = int(input("\nÉcrire combien de photos à choisir au hasard : "))
		
		self.filenamesPicked  = random.sample(self.listFilenames, self.numberOfFiles)

		print("\n")
		self.printfilename(self.filenamesPicked)



		self.fileCopyMsg = input("\nVoulez vous copier ces fichiers ? (O/N) ")

		if self.fileCopyMsg == 'O':
			# define the name of the directory to be created
			path = "RandomPhotos"

			try:  
				os.mkdir(path)
			except OSError:  
				print ("\nCouldn't create \"%s\" because it already exist" % path)
			else:  
				for item in self.filenamesPicked:
					filename = os.path.basename(item)
					copyfile(item, os.path.join(self.currentDirectory+"\\RandomPhotos", filename))
				


	def getFilenamesWithPath(self, source):
		listFilenames = []
		for dirpath, dirnames, filenames in os.walk(source):
			for filename in filenames:
				if filename.endswith('.jpg'):
					listFilenames.append(os.path.join(dirpath, filename))
		return listFilenames


	def getFilenames(self, source):
		listFilenames = []
		for dirpath, dirnames, filenames in os.walk(source):
			for filename in filenames:
				if filename.endswith('.jpg'):
					listFilenames.append(filename)
		return listFilenames

	def getCurrentDirectory(self):
		path = os.getcwd()
		return path

	def printfilename(self, listfilenames):
		for filename in listfilenames:
			print("\""+filename+"\"")

	def makeDir(self,name):
		os.mkdir(name)


if __name__ == '__main__':
	rpp=RandomPhotosPicker()
	rpp.main()