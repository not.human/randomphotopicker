# RandomPhotoPicker

Rpp is a python tool that allows you to make random photo selection.

## Getting Started

RandomPhotosPicker.py is a python script. You can run such a script on IDE (Eclipse, Visual Studio Code).

Or you can run it from the command prompt on windows as describe bellow.

### Prerequisites

Make sure to have python 3 installed on your machine. 

[https://www.python.org/downloads/](https://www.python.org/downloads/)

### How-to-run

Open Terminal, bash or Command Prompt

```
cd MyFolderWithPhotos
```

```
py -3 RandomPhotosPicker.py
```

## Versioning

Version 1.0

still working on it
 
## Authors

* **Antoine Girard Dubreuil**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Frantz
* Inspiration
* etc